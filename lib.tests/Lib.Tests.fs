﻿namespace Lib

open FsUnit
open Library
open Xunit

module Tests =
    [<Fact>]
    let ``Library Tests`` () : unit = 
        Core.f 1 |> should equal [|1; 1|]

    let ``Library Test Data`` : obj array seq = 
        [| (1, [|1; 1|])
           (2, [|2; 2|]) |]
        |> Seq.map (fun (a, b) -> 
               [| box a
                  box b |])

    [<Theory>]
    [<MemberData("Library Test Data")>]
    let ``Library Tests - Theory``(i:int, o:int[]) =
        Core.f i |> should equal o
