﻿// Learn more about F# at http://fsharp.org

open System
open Library

[<EntryPoint>]
let main argv =
    printfn "Hello World!"
    printfn "%A" (Core.f argv)
    0 // return an integer exit code
